"use strict";

var ZombieService = (function (PushService) {

  var service = PushService;
  var personifyCallbacks = {};
  var personifyOnceCallbacks = {};
  var personifiedDisplays = [];
  var registeredServiceCallbacks = {
      "personify" : false,
      "personifyOnce" : false,
  };

  service.registerPersonify = function (display, callback, zombie) {
    display = display.toLowerCase();
    service.registerCallbacks();
    if (!personifyCallbacks[display]) personifyCallbacks[display] = [];
    personifyCallbacks[display][zombie.id] = callback;
    return {
        deregister: function () {
          if (personifyCallbacks && personifyCallbacks[display])
            delete(personifyCallbacks[display][zombie.id]);
        }
    };
  };

  service.registerPersonifyOnce = function (display, callback, zombie) {
    display = display.toLowerCase();
    service.registerCallbacks();
    if (!personifyOnceCallbacks[display]) personifyOnceCallbacks[display] = [];
    personifyOnceCallbacks[display][zombie.id] = callback;
    return {
        deregister: function () {
          if (personifyOnceCallbacks && personifyOnceCallbacks[display])
            delete(personifyOnceCallbacks[display][zombie.id]);
        }
    };
  };

  service.registerCallbacks = function() {
    service.registerPersonifyCallback();
    service.registerPersonifyOnceCallback();
  };

  service.registerPersonifyCallback = function() {
    if (registeredServiceCallbacks["personify"]) return;
    registeredServiceCallbacks["personify"] = true;
    service.registerCallback("personify", service.onPersonify.bind(this));
  };

  service.registerPersonifyOnceCallback = function() {
      if (registeredServiceCallbacks["personifyOnce"]) return;
      registeredServiceCallbacks["personifyOnce"] = true;
      service.registerCallback("personifyOnce", service.onPersonifyOnce.bind(this));
  };

  service.onPersonify = function(parameters) {
    var display = parameters.display.toLowerCase();
    if (!personifyCallbacks[display]) return;

    for(var index in personifyCallbacks[display]) {
      if (!personifyCallbacks[display][index] instanceof Object) continue;
      var zombieCallback = personifyCallbacks[display][index];
      if (zombieCallback(parameters)) delete(personifyCallbacks[display][index]);
    }
  };

  service.onPersonifyOnce = function(parameters) {
    var display = parameters.display.toLowerCase();
    if (!personifyOnceCallbacks[display]) return;

    for(var index in personifyOnceCallbacks[display]) {
      if (!personifyOnceCallbacks[display][index] instanceof Object) continue;
      var zombieCallback = personifyOnceCallbacks[display][index];
      if (personifiedDisplays[parameters.id]) break;
      if (zombieCallback(parameters)) {
        personifiedDisplays.push(parameters.id);
        delete(personifyOnceCallbacks[display][index]);
      }
    }
  };

  return service;
})(PushService);
